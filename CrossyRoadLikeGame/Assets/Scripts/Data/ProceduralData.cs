﻿using Game.Containers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    public class ProceduralData : MonoBehaviour
    {
        public LevelData LevelData;
        public Vector2 LevelSize = new Vector2(10, 100);
        public int LevelWidth => (int)LevelSize.x;
        public int LevelHeight => (int)LevelSize.y;
        public float PercentToFill = 0.45f;
        public int IterationSteps = 100000;

        public LevelContainer LevelContainer;

        [Header("Prefabs")]
        public RoomPrefab[] RoomPrefabs;
        public GameObject WallPrefab;
    }

    public enum LevelType
    {
        Empty, Ground, Water, Wall, RoomSpawn
    }

    public class LevelData
    {
        public CellData[,] Cells;
        public class CellData
        {
            public LevelType Type;
            public Vector2 Pos;
        }
    }
}
