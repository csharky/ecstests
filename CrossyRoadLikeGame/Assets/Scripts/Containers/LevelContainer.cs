﻿using UnityEngine;
using Game.Controllers;
using System.Collections.Generic;
using System.Linq;

namespace Game.Containers
{
    public class LevelContainer : MonoBehaviour
    {
        public GroundContainer Ground;
        public WallsContainer Walls;
    }
}
