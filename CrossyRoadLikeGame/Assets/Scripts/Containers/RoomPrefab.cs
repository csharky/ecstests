﻿using UnityEngine;
using Game.Controllers;
using System.Collections.Generic;
using System.Linq;

namespace Game.Containers
{
    public class RoomPrefab : MonoBehaviour
    {
        private FloorPrefab[] _floorPrefabs;
        public FloorPrefab[] FloorPrefabs => _floorPrefabs != null ? _floorPrefabs : _floorPrefabs = GetComponentsInChildren<FloorPrefab>();
    }
}
