﻿using UnityEngine;
using Game.Controllers;
using System.Collections.Generic;
using System.Linq;

namespace Game.Containers
{
    public class FloorPrefab : MonoBehaviour
    {
        public Vector2 Pos => new Vector2(transform.position.x, transform.position.y);
    }
}
