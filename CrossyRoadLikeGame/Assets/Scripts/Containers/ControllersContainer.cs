﻿using UnityEngine;
using Game.Controllers;
using System.Collections.Generic;
using System.Linq;

namespace Game.Containers
{
    public class ControllersContainer : MonoBehaviour
    {
        private List<IController> _controllersList = new List<IController>();

        private IProceduralGeneratorController proceduralGeneratorController;
        public IProceduralGeneratorController ProceduralGeneratorController => proceduralGeneratorController != null
            ? proceduralGeneratorController
            : proceduralGeneratorController = GetComponentInChildren<ProceduralGeneratorController>();

        public void InitControllers()
        {
            _controllersList = GetComponentsInChildren<IController>().ToList();

            foreach (var controller in _controllersList)
            {
                controller.Init();
            }
        }
    }
}
