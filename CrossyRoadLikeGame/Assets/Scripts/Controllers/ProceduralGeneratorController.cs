﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Data;
using System.Linq;
using Game.Containers;

namespace Game.Controllers
{
    public class ProceduralGeneratorController : MonoBehaviour, IProceduralGeneratorController
    {
        public ProceduralData ProceduralData;

        private struct RandomWalker
        {
            public Vector2 dir;
            public Vector2 pos;
        }

        private List<RandomWalker> walkers;

        public void Init()
        {
            Setup();
            PrepareLevel();
            GenerateRooms();
            InstantiateRooms();
            //GenerateGrounds();
            //InstantiateGrounds();
            GenerateWalls();
            InstantiateWalls();
        }

        private void Setup()
        {
            ProceduralData = GetComponent<ProceduralData>();
            ProceduralData.LevelContainer = FindObjectOfType<LevelContainer>();
     }

        private void PrepareLevel()
        {
            ProceduralData.LevelData = new LevelData() {
                Cells = new LevelData.CellData[ProceduralData.LevelWidth, ProceduralData.LevelHeight]
            };

            for (int x = 0; x < ProceduralData.LevelWidth; x++)
            {
                for (int y = 0; y < ProceduralData.LevelHeight; y++)
                {
                    ProceduralData.LevelData.Cells[x, y] = new LevelData.CellData() { Type = LevelType.Empty, Pos = new Vector2(x, y) };
                }
            }

            walkers = new List<RandomWalker>();
            RandomWalker walker = new RandomWalker();
            walker.dir = RandomDirection();
            Vector2 pos = new Vector2(Mathf.RoundToInt(ProceduralData.LevelWidth * 0.5f), Mathf.RoundToInt(ProceduralData.LevelHeight * 0.5f));
            walker.pos = pos;
            walkers.Add(walker);
        }

        public float percentToFill = 0.2f;
        public float chanceWalkerChangeDir = 0.5f;
        public float chanceWalkerSpawn = 0.05f;
        public float chanceWalkerDestoy = 0.05f;
        public int maxWalkers = 10;
        public int iterationSteps = 100000;
        public int maxRooms = 10;

        private int numberOfGrounds = 0;


        private void GenerateRooms()
        {
            for(var i = 0; i < maxRooms; i++)
            {
                var randomPos = new Vector2Int(Random.Range(2, ProceduralData.LevelWidth - 2), Random.Range(2, ProceduralData.LevelHeight));

                ProceduralData.LevelData.Cells[randomPos.x, randomPos.y].Type = LevelType.RoomSpawn;
            }
        }

        private void GenerateGrounds()
        {
            int iterations = 0;
            do
            {
                //create Ground at position of every Walker
                foreach (RandomWalker walker in walkers)
                {
                    ProceduralData.LevelData.Cells[(int)walker.pos.x, (int)walker.pos.y].Type = LevelType.Ground;
                    numberOfGrounds += 4;
                }

                //chance: destroy Walker
                int numberChecks = walkers.Count;
                for (int i = 0; i < numberChecks; i++)
                {
                    if (Random.value < chanceWalkerDestoy && walkers.Count > 1)
                    {
                        walkers.RemoveAt(i);
                        break;
                    }
                }

                //chance: Walker pick new direction
                for (int i = 0; i < walkers.Count; i++)
                {
                    if (Random.value < chanceWalkerChangeDir)
                    {
                        RandomWalker thisWalker = walkers[i];
                        thisWalker.dir = RandomDirection();
                        walkers[i] = thisWalker;
                    }
                }

                //chance: spawn new Walker
                numberChecks = walkers.Count;
                for (int i = 0; i < numberChecks; i++)
                {
                    if (Random.value < chanceWalkerSpawn && walkers.Count < maxWalkers)
                    {
                        RandomWalker walker = new RandomWalker();
                        walker.dir = RandomDirection();
                        walker.pos = walkers[i].pos;
                        walkers.Add(walker);
                    }
                }

                //move Walkers
                for (int i = 0; i < walkers.Count; i++)
                {
                    RandomWalker walker = walkers[i];
                    walker.pos += walker.dir;
                    walkers[i] = walker;
                }

                //avoid boarder of ProceduralData.LevelData.Cells
                for (int i = 0; i < walkers.Count; i++)
                {
                    RandomWalker walker = walkers[i];
                    walker.pos.x = Mathf.Clamp(walker.pos.x, 1, ProceduralData.LevelWidth - 2);
                    walker.pos.y = Mathf.Clamp(walker.pos.y, 1, ProceduralData.LevelHeight - 2);
                    walkers[i] = walker;
                }

                //check to exit loop
                if ((float)numberOfGrounds / (float)ProceduralData.LevelData.Cells.Length > percentToFill)
                {
                    break;
                }
                iterations++;
            } while (iterations < iterationSteps);
        }

        private void GenerateWalls()
        {
            for (int x = 0; x < ProceduralData.LevelWidth; x++)
            {
                for (int y = 0; y < ProceduralData.LevelHeight; y++)
                {
                    if (ProceduralData.LevelData.Cells[x, y].Type == LevelType.Ground)
                    {
                        if (ProceduralData.LevelData.Cells[x, y + 1].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x, y + 1].Type = LevelType.Wall;
                        }

                        if (ProceduralData.LevelData.Cells[x, y - 1].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x, y - 1].Type = LevelType.Wall;
                        }
                        if (ProceduralData.LevelData.Cells[x + 1, y].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x + 1, y].Type = LevelType.Wall;
                        }
                        if (ProceduralData.LevelData.Cells[x - 1, y].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x - 1, y].Type = LevelType.Wall;
                        }

                        if (ProceduralData.LevelData.Cells[x - 1, y - 1].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x - 1, y - 1].Type = LevelType.Wall;
                        }
                        if (ProceduralData.LevelData.Cells[x - 1, y + 1].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x - 1, y + 1].Type = LevelType.Wall;
                        }
                        if (ProceduralData.LevelData.Cells[x + 1, y + 1].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x + 1, y + 1].Type = LevelType.Wall;
                        }
                        if (ProceduralData.LevelData.Cells[x + 1, y - 1].Type == LevelType.Empty)
                        {
                            ProceduralData.LevelData.Cells[x + 1, y - 1].Type = LevelType.Wall;
                        }
                    }
                }
            }
        }

        private void InstantiateRooms()
        {
            foreach (var cell in ProceduralData.LevelData.Cells)
            {
                if (cell.Type == LevelType.RoomSpawn)
                {
                    var roomPrefab = ProceduralData.RoomPrefabs[Random.Range(0, ProceduralData.RoomPrefabs.Length)];
                    var groundPrefab = Instantiate(roomPrefab, ProceduralData.LevelContainer.Ground.transform);
                    groundPrefab.transform.position = new Vector3(cell.Pos.x, 0, cell.Pos.y);

                    foreach (var floor in groundPrefab.FloorPrefabs)
                    {
                        var pos = cell.Pos + floor.Pos;

                        if (ProceduralData.LevelData.Cells.GetLength(0) > (int)pos.x && ProceduralData.LevelData.Cells.GetLength(1) > (int)pos.y)
                        {
                            ProceduralData.LevelData.Cells[(int)pos.x, (int)pos.y].Type = LevelType.Ground;
                        }
                    }
                }
            }
        }

        private void InstantiateGrounds()
        {
            foreach (var cell in ProceduralData.LevelData.Cells)
            {
                if (cell.Type == LevelType.Ground)
                {
                    var roomPrefab = ProceduralData.RoomPrefabs[Random.Range(0, ProceduralData.RoomPrefabs.Length - 1)];
                    var groundPrefab = Instantiate(roomPrefab, ProceduralData.LevelContainer.Ground.transform);
                    groundPrefab.transform.position = new Vector3(cell.Pos.x, 0, cell.Pos.y);
                    groundPrefab.transform.Rotate( RandomDirection() * 90f );

                    foreach(var floor in groundPrefab.FloorPrefabs)
                    {
                        var pos = cell.Pos + floor.Pos;

                        if (ProceduralData.LevelData.Cells.GetLength(0) > (int)pos.x && ProceduralData.LevelData.Cells.GetLength(1) > (int)pos.y)
                        {
                            ProceduralData.LevelData.Cells[(int)pos.x, (int)pos.y].Type = LevelType.Ground;
                        }
                    }
                }
            }
        }

        private void InstantiateWalls()
        {
            foreach (var cell in ProceduralData.LevelData.Cells)
            {
                if (cell.Type == LevelType.Wall)
                {
                    var GroundPrefab = Instantiate(ProceduralData.WallPrefab, ProceduralData.LevelContainer.Walls.transform);
                    GroundPrefab.transform.position = new Vector3(cell.Pos.x, 0, cell.Pos.y);
                }
            }
        }

        private Vector2 RandomDirection()
        {
            int choice = Mathf.FloorToInt(Random.value * 3.99f);
            switch (choice)
            {
                case 0:
                    return Vector2.down * 2;
                case 1:
                    return Vector2.left * 2;
                case 2:
                    return Vector2.up * 2;
                default:
                    return Vector2.right * 2;
            }
        }
    }

    public interface IProceduralGeneratorController : IController
    {

    }
}

