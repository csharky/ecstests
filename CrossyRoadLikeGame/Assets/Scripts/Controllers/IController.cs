﻿using UnityEngine;

namespace Game.Controllers
{
    public abstract class MonoController<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T New()
        {
            var go = new GameObject();
            go.name = typeof(T).ToString() + " (MonoController)";
            return go.AddComponent<T>();
        }
    }

    public interface IController : IInjectable
    {
        void Init();
    }

    public interface IInjectable
    {
    }
}
