﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Containers;

namespace Game.Controllers
{
    public class GameController : MonoBehaviour, IController
    {
        private ControllersContainer ControllersContainer;
        public void Init()
        {
            ControllersContainer = FindObjectOfType<ControllersContainer>();

            ControllersContainer.InitControllers();
        }

        private void Awake()
        {
            Init();
        }
    }
}

