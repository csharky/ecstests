﻿using Game.Utils;
using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Game.ECS.Components
{
    [Serializable]
    public struct UnitHighlight : IComponentData
    {
        public Color Normal;
        public Color Highlight; 
    }
    
    public class UnitHighlightComponent : ComponentDataProxy<UnitHighlight>
    { }
}
