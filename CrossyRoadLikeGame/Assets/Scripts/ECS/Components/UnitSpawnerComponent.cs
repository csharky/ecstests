﻿using Game.Utils;
using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Game.ECS.Components
{
    [Serializable]
    public struct UnitSpawner : IComponentData
    {
        public Entity Prefab;
        public int Rows;
        public int Cols;
    }
}
