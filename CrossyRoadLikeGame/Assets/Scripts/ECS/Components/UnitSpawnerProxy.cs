using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Game.ECS.Components
{
    public class UnitSpawnerProxy : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
    {
        public GameObject Prefab;
        public int Rows;
        public int Cols;
        
        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.Add(Prefab);
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var spawnerData = new UnitSpawner()
            {
                Prefab = conversionSystem.GetPrimaryEntity(Prefab),
                Rows = Rows,
                Cols = Cols
            };

            dstManager.AddComponentData(entity, spawnerData);
        }
    }
}