﻿using Game.Utils;
using Unity.Entities;
using Unity.Mathematics;

namespace Game.ECS.Components
{
    public enum NavAgentStatus { Idle, Moving }

    [System.Serializable]
    public struct NavAgent : IComponentData
    {
        public float3 FinalDestination;
        public NavAgentStatus NavAgentStatus;
    }
    
    public class NavAgentComponent : ComponentDataProxy<NavAgent> { }
}
