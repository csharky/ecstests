﻿using Game.Utils;
using Unity.Entities;
using Unity.Mathematics;

namespace Game.ECS.Components
{
    [System.Serializable]
    public struct UnitSelect : IComponentData
    {
        public bool IsSelected;
    }
    
    public class UnitSelectComponent : ComponentDataProxy<UnitSelect> { }
}
