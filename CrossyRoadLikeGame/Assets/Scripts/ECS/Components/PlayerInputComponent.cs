﻿using Game.Utils;
using Unity.Entities;
using Unity.Mathematics;

namespace Game.ECS.Components
{
    [System.Serializable]
    public struct PlayerInput : IComponentData
    {
        public bool LeftClick;
        public bool RightClick;
        public float3 MousePosition;
    }
    
    public class PlayerInputComponent : ComponentDataProxy<PlayerInput> { }
}
