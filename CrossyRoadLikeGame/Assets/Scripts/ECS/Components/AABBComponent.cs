﻿using Game.Utils;
using Unity.Entities;
using Unity.Mathematics;

namespace Game.ECS.Components
{
    [System.Serializable]
    public struct AABB : IComponentData
    {
        public float3 Min;
        public float3 Max;
    }
    
    public class AABBComponent : ComponentDataProxy<AABB> { }
}
