﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Transforms;
using AABB = Game.ECS.Components.AABB;

namespace Game.ECS.Systems
{
    public class AABBCollisionSystem : JobComponentSystem
    {
        [BurstCompile]
        struct AABBCollisionJob : IJobParallelFor
        {
            [ReadOnly] public NativeArray<AABB> Colliders;
            
            public void Execute(int j)
            {
                for (int i = 0; i < Colliders.Length - 1; i++)
                {
                    if (i != j)
                    {
                        if (RTSPhysics.Intersect(Colliders[i], Colliders[j]))
                        {
                            //Debug.Log("Collision detected!");
                        }
                    }

                }
            }
        }

        public EntityQuery _AABBQuery;

        protected override void OnCreateManager()
        {
            var query = new EntityQueryDesc()
            {
                All = new ComponentType[] { typeof(AABB) }
            };

            _AABBQuery = GetEntityQuery(query);
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var colliders = _AABBQuery.ToComponentDataArray<AABB>(Allocator.TempJob);
            var job = new AABBCollisionJob()
            {
                Colliders = colliders
            };
            var collisionJobHandler = job.Schedule(colliders.Length, 32);
            collisionJobHandler.Complete();
            
            colliders.Dispose();
            return collisionJobHandler;
        }
    }
    
}
