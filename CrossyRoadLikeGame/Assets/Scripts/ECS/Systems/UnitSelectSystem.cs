﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Transforms;
using Unity.Collections;
using AABB = Game.ECS.Components.AABB;

namespace Game.ECS.Systems
{
    public class UnitSelectSystem : JobComponentSystem
    {
        private EntityCommandBufferSystem _barrier;

        struct UnitSelectJob : IJobForEachWithEntity<PlayerInput, Translation, AABB>
        {
            public EntityCommandBuffer.Concurrent CommandBuffer;
            [ReadOnly] public ComponentDataFromEntity<UnitSelect> Selected;
            public Ray Ray;

            public void Execute(Entity entity, int index, [ReadOnly] ref PlayerInput pInput, 
                [ReadOnly] ref Translation translation, ref AABB aabb)
            {
                if (pInput.LeftClick)
                {
                    if (Selected.Exists(entity))
                    {
                        CommandBuffer.RemoveComponent<UnitSelect>(index, entity);
                    }

                    if (RTSPhysics.Intersect(aabb, Ray))
                    {
                        CommandBuffer.AddComponent(index, entity, new UnitSelect());
                    }
                }
            }
        }

        protected override void OnCreate()
        {
            _barrier = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var commandBuffer = _barrier.CreateCommandBuffer().ToConcurrent();
            var selected = GetComponentDataFromEntity<UnitSelect>();

            var job = new UnitSelectJob
            {
                CommandBuffer = commandBuffer,
                Selected = selected,
                Ray = Camera.main.ScreenPointToRay(Input.mousePosition)
            };

            inputDeps = job.Schedule(this, inputDeps);
            _barrier.AddJobHandleForProducer(inputDeps);

            return inputDeps;
        }
    }

}
