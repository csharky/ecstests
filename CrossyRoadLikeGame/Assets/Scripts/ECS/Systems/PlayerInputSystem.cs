﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;

namespace Game.ECS.Systems
{
    public class PlayerInputSystem : JobComponentSystem
    {
        struct PlayerInputJob : IJobForEach<PlayerInput>
        {
            public bool LeftClick;
            public bool RightClick;
            public float3 MousePosition;

            public void Execute(ref PlayerInput data)
            {
                data.LeftClick = LeftClick;
                data.RightClick = RightClick;
                data.MousePosition = MousePosition;
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var mousePos = Input.mousePosition;
            var ray = Camera.main.ScreenPointToRay(mousePos);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                mousePos = new float3(hit.point.x - 1.7f, 0, hit.point.z + 3f);
            }

            var job = new PlayerInputJob();
            job.LeftClick = Input.GetMouseButtonDown(0);
            job.RightClick = Input.GetMouseButton(1);
            job.MousePosition = mousePos;

            return job.Schedule(this, inputDeps);
        }
    }
    
}
