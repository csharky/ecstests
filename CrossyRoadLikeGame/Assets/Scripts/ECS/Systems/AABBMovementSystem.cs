﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Transforms;
using AABB = Game.ECS.Components.AABB;

namespace Game.ECS.Systems
{
    public class AABBMovementSystem : JobComponentSystem
    {
        struct AABBMovementJob : IJobForEach<AABB, Translation>
        {
            public void Execute(ref AABB aabbData, ref Translation translation)
            {
                aabbData.Max = translation.Value + 0.5f;
                aabbData.Min = translation.Value - 0.5f;
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new AABBMovementJob();
            return job.Schedule(this, inputDeps);
        }
    }
    
}
