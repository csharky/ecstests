﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Transforms;

namespace Game.ECS.Systems
{
    public class NavAgentMovementSystem : JobComponentSystem
    {
        struct NavAgentMovementJob : IJobForEach<Translation, Rotation, NavAgent>
        {
            public float DeltaTime;

            public void Execute(ref Translation translation, ref Rotation rotation, ref NavAgent navAgent)
            {
                var distance = math.distance(navAgent.FinalDestination, translation.Value);

                if (navAgent.NavAgentStatus == NavAgentStatus.Moving)
                {
                    var direction = math.normalize(navAgent.FinalDestination - translation.Value);

                    if (!(distance <= 1))
                    {
                        translation.Value += direction * 5f * DeltaTime;
                    }
                    else
                    {
                        navAgent.NavAgentStatus = NavAgentStatus.Idle;
                    }

                    rotation.Value = Quaternion.LookRotation(direction);
                }
                
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new NavAgentMovementJob()
            {
                DeltaTime = Time.deltaTime
            };

            return job.Schedule(this, inputDeps);
        }
    }
    
}
