﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Transforms;

namespace Game.ECS.Systems
{
    public class PlayerUnitMovementSystem : JobComponentSystem
    {
        struct PlayerUnitMovementJob : IJobForEach<PlayerInput, NavAgent, UnitSelect>
        {
            public bool LeftClick;
            public bool RightClick;
            public float3 MousePosition;
            public float3 PlayerPosition;

            public void Execute(ref PlayerInput playerInputData, 
                ref NavAgent navAgent,
                ref UnitSelect unitSelect)
            {
                if (playerInputData.RightClick)
                {
                    navAgent.FinalDestination = playerInputData.MousePosition;
                    navAgent.NavAgentStatus = NavAgentStatus.Moving;
                }
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new PlayerUnitMovementJob();

            return job.Schedule(this, inputDeps);
        }
    }
    
}
