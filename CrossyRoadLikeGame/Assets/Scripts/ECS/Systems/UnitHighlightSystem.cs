﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Transforms;
using Unity.Rendering;

namespace Game.ECS.Systems
{
    public class UnitHighlightSystem : ComponentSystem
    {
        private EntityQuery _selected;

        protected override void OnCreateManager()
        {
            _selected = GetEntityQuery(typeof(RenderMesh), typeof(UnitHighlight), typeof(UnitSelect));
        }

        protected override void OnUpdate()
        {
        }
    }

}
