﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Collections;
using Unity.Transforms;

namespace Game.ECS.Systems
{
    public class UnitSpawnerSystem : JobComponentSystem
    {
        struct UnitSpawnerJob : IJobForEachWithEntity<UnitSpawner, LocalToWorld>
        {
            public EntityCommandBuffer CommandBuffer;
            
            public void Execute(Entity entity, int index, [ReadOnly] ref UnitSpawner spawner, [ReadOnly] ref LocalToWorld localToWorld)
            {
                for (var i = 0; i < spawner.Rows; i++)
                {
                    for (var j = 0; j < spawner.Cols; j++)
                    {
                        var instance = CommandBuffer.Instantiate(spawner.Prefab);

                        var position = math.transform(
                            localToWorld.Value, 
                            new float3(2 * i, 0, 2 * j)
                        );
                        
                            
                        CommandBuffer.SetComponent(instance, new Translation() { Value = position });

                        var aabb = new Components.AABB()
                        {
                            Max = position + 0.5f,
                            Min = position - 0.5f
                        };
                        
                        CommandBuffer.AddComponent(instance, aabb);
                        CommandBuffer.AddComponent(instance, new PlayerInput());
                        CommandBuffer.AddComponent(instance, new NavAgent());
                    }
                }
                
                CommandBuffer.DestroyEntity(entity);
            }
        }

        public BeginInitializationEntityCommandBufferSystem _CommandBufferSystem;

        protected override void OnCreateManager()
        {
            _CommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new UnitSpawnerJob()
            {
                CommandBuffer = _CommandBufferSystem.CreateCommandBuffer()
            }.ScheduleSingle(this, inputDeps);
            
            _CommandBufferSystem.AddJobHandleForProducer(job);
            return job;
        }
    }

}
