﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Game.ECS.Components;
using Game.Utils;
using Unity.Transforms;

namespace Game.ECS.Systems
{
    public class PlayerUnitRotationSystem : JobComponentSystem
    {
        struct PlayerUnitRotationJob : IJobForEach<PlayerInput, Translation, Rotation, UnitSelect, NavAgent>
        {
            public void Execute(ref PlayerInput playerInputData, ref Translation translation, ref Rotation rotation, ref UnitSelect unitSelect, ref NavAgent navAgent)
            {
                if (navAgent.NavAgentStatus != NavAgentStatus.Moving)
                {
                    var direction = math.normalize(playerInputData.MousePosition - translation.Value);
                    rotation.Value = Quaternion.LookRotation(direction);
                }
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new PlayerUnitRotationJob();

            return job.Schedule(this, inputDeps);
        }
    }
    
}
